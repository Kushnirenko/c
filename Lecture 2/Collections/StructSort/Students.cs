﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace StructSort
{
    struct Students : IComparable<Students>
    {
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Group { get; set; }
        public string Age { get; set; }
        public static List<Students> students = new List<Students>();

        public int CompareTo(Students other)
        {
            return (FirstName.CompareTo(other.FirstName));
        }

        public override string ToString()
        {
            return (String.Format("name: {0} LastName: {1} Group:{2}, Age:{3}", FirstName, LastName, Group, Age));
        }

        private void Init()
        {
            students.Add(new Students() { FirstName = "Ivan", LastName = "Petrov", Group = "kn10-1", Age = "20" });
            students.Add(new Students() { FirstName = "Antin", LastName = "Ivanov", Group = "kn10-1", Age = "21" });
            students.Add(new Students() { FirstName = "Eugene", LastName = "Sidorov", Group = "kn10-1", Age = "22" });
        }

        public void Print()
        {
            Init();
            foreach (var item in students)
            {
                Console.WriteLine(item);
            }
            Console.ReadKey();
        }

    }
}
