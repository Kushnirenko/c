﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Task_2
{
    class Program
    {
        static void Main(string[] args)
        {
            var stud = new Student();
            
            var ipr = (IPrintable) stud;
            stud.Stud.Sort();
            ipr.Print();
            
            Console.ReadKey();
        }
    }
}
