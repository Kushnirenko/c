﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Task_2
{
    public class Student:Person,IPrintable,IComparable<Student>
    {
        public static string ByGroup { get; set; }

        public class StudentComparer:IComparer<Student>
        {
            public StudentComparer(string by)
            { }

            public int Compare(Student x, Student y)
            {
                return Compare(x, y);
            }
        }

        public  string Group { get; set; }

        public Student() {}

        public List<Student> Stud = new List<Student>();

        void IPrintable.Print()
        {
            Stud.Add(new Student { Name = "Петр", Age = 20, Group = "KN 10-1" });
            Stud.Add(new Student { Name = "Иван", Age = 18, Group = "KN 12-1" }); 

            foreach (Student item in Stud)
            {
                Console.WriteLine("{0} {1} {2}",item.Name,item.Group,item.Age);
            }
        }


        public int CompareTo(Student other)
        {
            return (Age.CompareTo(other.Age));
        }
    }
}
