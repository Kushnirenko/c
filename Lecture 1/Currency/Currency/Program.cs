﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Currency
{
    class Program
    {
        enum Currency { dollar, euro };

        static void Main(string[] args)
        {
            double[] kurs = { 8, 10 };
            Console.Write("Введите название валюты ");
            string n = Console.ReadLine();
            Currency choose;
            try
            {
                choose = (Currency)Enum.Parse(typeof(Currency), n);

                if (choose == Currency.dollar)
                {
                    Console.WriteLine("Введите количество меняемых денежных единиц ");
                    double value = Convert.ToDouble(Console.ReadLine());
                    value*=8.15;
                    Console.WriteLine("ровно "+value+" гривен");
                }
                if (choose == Currency.euro)
                {
                    Console.WriteLine("Введите количество меняемых денежных единиц ");
                    double value = Convert.ToDouble(Console.ReadLine());
                    value *= 11.50;
                    Console.WriteLine("ровно " + value + " гривен");
                }

            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message); 
            }

            Console.ReadKey();
        }
    }
}
