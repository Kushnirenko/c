﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Task_1
{
    class Teacher : Person
    {
        private string _degree;

        public Teacher() { }

        public Teacher(string fio, int age, string degree)
        {
            _Fio = fio;
            _Age = age;
            _degree = degree;
        }
    }
}
