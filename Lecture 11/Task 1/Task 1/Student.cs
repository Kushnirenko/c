﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Task_1
{
    public class Student : Person
    {
        private string _group;

        public Student() { }

        public Student(string fio, int age, string group)
        {
            _Fio = fio;
            _Age = age;
            _group = group;
        }  
    }
}
