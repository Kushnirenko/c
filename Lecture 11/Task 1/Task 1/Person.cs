﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Task_1
{
    public class Person
    {
        protected string _Fio;
        protected int _Age;
        public static List<Person> pers = new List<Person>();

        public Person()
        {
            
            Person.pers.Add(new Person("Petrov", 22));
            Person.pers.Add(new Person("Ivanov", 23));
        }

        public Person(string Fio, int Age)
        {
            _Fio = Fio;
            _Age = Age;
        }

        public static bool operator ==(Person firstAge,Person secondAge)
        {
            return firstAge==secondAge;
        }
        public static bool operator !=(Person firstAge, Person secondAge)
        {
            return firstAge != secondAge;
        }

        public void ToLiken()
        {
            Person p = new Person();
            p = pers[0];
            Person p2 = new Person();
            p2 = pers[1];
            if (p._Age == p2._Age)
            {
                Console.WriteLine("Обьекты равны по возрасту");
            }
            else {
                Console.WriteLine("Обьекты неравны по возрасту");
            }
        }

    }   
}

