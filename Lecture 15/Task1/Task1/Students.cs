﻿using System;
using System.Collections.Generic;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.Serialization.Formatters.Binary;
using System.Runtime.Serialization.Formatters.Soap;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;

namespace Task1
{
    [Serializable]
    public class Students
    {
        public string _firstName { get; set; }
        public string _name { get; set; }
        public int _age { get; set; }
        public string _group { get; set; }

        public static List<Students> st = new List<Students>() { 
            new Students{ _firstName = "Ivanov", _name = "Sergey", _age = 18,_group = "KN-10-1" },
            new Students{_firstName = "Petrov",_name = "Ivan",_age = 20,_group = "KN-09-1"}
        };
        
        public  Students() { }

        private void PrintList(List<Students> data)
        {
            foreach (Students item in data)
            {
                Console.WriteLine("{0,11} {1,10} {2,11} {3,3}", item._firstName, item._name, item._group, item._age);
            }
        }


        public void BinnarySerialization()
        {

            FileStream fs = new FileStream("SerializedString.Data", FileMode.Create);
            BinaryFormatter bf = new BinaryFormatter();
            bf.Serialize(fs, Students.st);
            fs.Close();
        }

        public void BinnaryDeSerialization()
        {
            Console.WriteLine("Binary");
            FileStream fsDes = new FileStream("SerializedString.Data", FileMode.Open);
            BinaryFormatter bfDes = new BinaryFormatter();
            var data = (List<Students>)bfDes.Deserialize(fsDes);
            fsDes.Close();
            PrintList(data);
        }

        public void XmlSeriazable()
        {
            FileStream fsXml = new FileStream("SerializedDate.XML", FileMode.Create);
            XmlSerializer xs = new XmlSerializer(typeof(List<Students>));
            xs.Serialize(fsXml, Students.st);
            fsXml.Close();
            
        }

        public void XmlDeSeriazable()
        {
            Console.WriteLine("Xml deserialize");
            XmlSerializer xs = new XmlSerializer(typeof(List<Students>));
            FileStream fsXmlDes = new FileStream("SerializedDate.XML", FileMode.Open);
            var desXml = (List<Students>)xs.Deserialize(fsXmlDes);
            PrintList(desXml);
            fsXmlDes.Close();
        }
 
        public void SoapSerialization()
        {
            FileStream soap = new FileStream("SerializedSoap.XML", FileMode.Create);
            SoapFormatter soapf = new SoapFormatter();
            soapf.Serialize(soap, st.ToArray());
            soap.Close();
        }
        public void SoapDeSerialization()
        {
            SoapFormatter soapf = new SoapFormatter();
            Console.WriteLine("Soap");
            FileStream soapDes = new FileStream("SerializedSoap.XML", FileMode.Open);
            Students[] masSt = (Students[])soapf.Deserialize(soapDes);
            foreach (var item in masSt)
            {
                Console.WriteLine("{0,11} {1,10} {2,11} {3,3}", item._firstName, item._name, item._group, item._age);
            }
        }
        

    }
}
