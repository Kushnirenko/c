﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;

namespace CompactDisk
{
   public class Disk
    {
       public Dictionary<Albums,List<Songs>> dict = new Dictionary<Albums, List<Songs>>();
      
       public void AddDisk(string album,string artist,List<Songs> songs)
       {
           var obj = new Albums { AlbumName = album, Artist = artist };
           dict.Add(obj,songs);
       }

       public void Search( string songName )
       {
           foreach (var item in dict)
           {
               foreach (var song in item.Value)
               {
                   if (songName == song.Song)
                   {
                       Console.WriteLine("Found song {1}: {0}",song.Song,item.Key.Artist); 
                   }
               }
           }
       }

       public void AddSong(string song, string artist)
       {
           var newSongs = new Songs
           {
               Song = song
           };
           foreach (var item in dict)
           {
               if (artist == item.Key.Artist)
               {
                   item.Value.Add(newSongs);
               }
           }
       }

       public void RemoveSong( string songName )
       {
           var songObj = new Songs
           {
               Song = songName
           };
           foreach (var item in dict)
           {
               foreach (var song in item.Value)
               {
                   if (songObj.Song == song.Song)
                   {
                       item.Value.Remove(song);
                       break;
                   }
                   else
                   {
                       Console.WriteLine("Song not find");
                   }
               }
           }
       }

       public void Print()
       {
           foreach(KeyValuePair<Albums,List<Songs>> item in dict)
           {
               Console.WriteLine("Album: {0}",item.Key.AlbumName);
               foreach (var songs in item.Value)
               {
                    Console.WriteLine("Artist:{0,11}  Song Name{1,15}",item.Key.Artist,songs.Song);   
               }
             
           }
       }

    }
}
