﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CompactDisk
{
    class Program
    {
        static void Main(string[] args)
        {
            var mysongs = new List<Songs>
            {
                new Songs{ Song = "2"},
                new Songs{ Song = "somesong2"}
            };

            var disk = new Disk();
            disk.AddDisk("album","Eminem", mysongs);
            disk.RemoveSong("2");
            disk.AddSong("newsong", "Eminem");
            disk.Print();
            disk.Search("newsong");
            Console.ReadKey();
        }
    }
}
