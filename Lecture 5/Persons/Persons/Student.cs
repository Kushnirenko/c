﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Persons
{
    public class Student:Person
    {
        private string _group;

        public Student() { }

        public Student(string fio, int age,string group)
        {
            _Fio = fio;
            _Age = age;
            _group = group;   
        }

        public override void Print()
        {
            Console.WriteLine("The Teachers of Students");
            foreach (var item in pers)
            {
                if(item is Teacher)
                Console.WriteLine(item);   
            }
        }
        public override string ToString()
        {
            return (String.Format("FIO: {0}\n Age: {1} Group:{2}\n", _Fio, _Age,_group));
        }
    }
}
