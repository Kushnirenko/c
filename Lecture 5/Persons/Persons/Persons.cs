﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Persons
{
    public class Person:IComparable<Person>
    {
        protected string _Fio;
        protected int _Age;
        public static List<Person> pers = new List<Person>();

        public Person() { }

        public Person(string Fio, int Age)
        {
            _Fio = Fio;
            _Age = Age;
        }       

        public virtual void Print()
        {
            int t = 0;
            int s = 0;
            foreach (var item in pers)
            {
                Console.WriteLine(item);
                if (item is Teacher)
                {
                    t++;
                }
                if (item is Student)
                {
                    s++;
                }
            }
            Console.WriteLine("Teachers count: {0}",t);
            Console.WriteLine("Students count: {0}", s);
        }

        public override string ToString()
        {
            return (String.Format("FIO: {0}\n Age: {1}\n", _Fio, _Age));
        }

        public int CompareTo(Person other)
        {
            return (_Fio.CompareTo(other._Fio));
        }
    }

}
