﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Persons
{
    class Program
    {
        static void Main(string[] args)
        {
            Person p = new Person();
            InitPersons();
            p.Print();
            Console.WriteLine();
            Console.ReadKey();
        }
        
        private static void InitPersons()
        {
            Person.pers.Add(new Teacher("Teacher1", 45, "degree"));
            Person.pers.Add(new Student("Student", 16, "KN10-1"));
            Person.pers.Add(new Teacher("Teacher2", 35, "phd"));
        }
    }
}
