﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Persons
{
    class Teacher:Person
    {
        private string _degree;

        public Teacher() { }

        public Teacher(string fio,int age,string degree)
        {
            _Fio = fio;
            _Age = age;
            _degree = degree;
        }

        public override void Print()
        {
            Console.WriteLine("The Students of Teachers");
            foreach (var item in pers)
            {
                if(item is Student)
                Console.WriteLine(item);
            }
        }

        public override string ToString()
        {
            return (String.Format("FIO: {0}\n Age: {1} Degree:{2}", _Fio, _Age,_degree));
        }
    }
}
