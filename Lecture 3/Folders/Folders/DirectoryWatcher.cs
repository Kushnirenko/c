﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Folders
{
    public static class DirectoryWatcher
    {
        static DirectoryInfo dir = new DirectoryInfo(@"..\..\..\..\..\");

        public static void GetCatalog()
        {
            Console.WriteLine("Список каталогов");
            foreach (var item in dir.GetDirectories())
            {
                Console.WriteLine(item.Name);
                Console.WriteLine("Список подкаталогов");
                foreach (var it in item.GetDirectories())
                    Console.WriteLine(it.Name);
                Console.WriteLine();
            }
        }

        public static void GetFiles()
        {
            Console.WriteLine("Список файлов");
            foreach (var item in dir.GetFiles())
            {
                Console.WriteLine(item.Name);
            }
        }
    }
}
