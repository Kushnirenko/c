﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TypeFile
{
    class TypeFile
    {
        string _temp = "", _str = "";

        private void ReadFile()
        {
            using (StreamReader reader = new StreamReader("list.txt"))
            {
                while ((_temp = reader.ReadLine()) != null)
                {
                    _str += _temp;
                }
                reader.Close();
            }
        }

        public void GetSquare()
        {
            string[] split = new string[_str.Length];
            int res;
            int[] square = new int[split.Length];
            string[] lines = new string[square.Length];

            ReadFile();

            for (int i = 0; i < _str.Length - 1; i++)
            {
                split = _str.Split(' ');
            }

            for (int i = 0; i < square.Length; i++)
            {
                if (int.TryParse(split[i], out res))
                {
                    square[i] = Convert.ToInt32(split[i]) * Convert.ToInt32(split[i]);
                    lines[i] = square[i].ToString();
                }
            }
            using (StreamWriter sw = new StreamWriter("list.txt"))
            {
                for (int i = 0; i < square.Length; i++)
                {
                    sw.Write(square[i] + " ");
                }

                sw.Close();
                Console.WriteLine(" File writed ");
            }
           
        }
    }
}
