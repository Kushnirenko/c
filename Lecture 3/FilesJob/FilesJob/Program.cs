﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FilesJob
{
    class Program
    {
        static void Main(string[] args)
        {
            FileJob fj = new FileJob();
            fj.ReadFile();
            fj.Summ();
            Console.ReadKey();
        }
    }
}
