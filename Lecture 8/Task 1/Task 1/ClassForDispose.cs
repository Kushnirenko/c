﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Task_1
{
    public class ClassForDispose:IDisposable
    {
                                             
        public ClassForDispose() { }

        public  ClassForDispose(string a)
        {
            Console.WriteLine("Ресурсы освобождены");
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        ~ClassForDispose() { }
        public virtual void Dispose(Boolean disposing)
        {
            if (disposing)
            {
                disposing = false;
            }
        }
    }
    public class HeritableClass:ClassForDispose
    {
        public override void Dispose(bool disposing)
        {
            try
            {
                if (disposing)
                {
                    disposing = false;
                }
            }
           finally
            {
                base.Dispose(disposing);
            }
        }
    }
}
