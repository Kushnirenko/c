﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Task_1
{
    public class MainClass
    {
        public MainClass() { }

    }
    public class ManagedClass:MainClass,IDisposable
    {
        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);  
        }
        protected virtual void Dispose(Boolean dispossing)
        {
            if (dispossing)
            {
                dispossing = false;
            }
        }
    }
}
