﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Shop
{
    class Customer
    {
        public string _email;
        public string _phone;
        public string _name;
        public string _address;
        public int _id;
        
        public Customer() { }

       public Customer(string email,string phone,string name,string address,int id)
        {
            _email = email;
            _phone = phone;
            _name = name;
            _address = address;
            _id = id;
        }

    }
}
