﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Shop
{
    class Program
    {
        static void Main(string[] args)
        {
            LinqQueryies linqq = new LinqQueryies();
            linqq.GetEmployeers();
            Console.WriteLine();
            linqq.GetCustomers();

            Console.ReadKey();

        }
    }
}
