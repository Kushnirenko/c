﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Shop
{
    class Employee
    {
        public string FirstName{get;set;}
        public string LastName { get; set; }
        public string Bdate{ get; set; }
        public string Post{ get; set; }
        public int Id{ get; set; }
        public string UserName{ get; set; }
        
        public Employee() { }

        public  Employee(string firstName,string lastName,string bDate,string post,int id,string userName)
        {
            FirstName = firstName;
            LastName = lastName;
            Bdate = bDate;
            Post = post;
            Id = id;
            UserName = userName;
        }

    }
}
