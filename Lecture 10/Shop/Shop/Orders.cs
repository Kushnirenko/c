﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Shop
{
    class Orders
    {
        public string _product;
        public double _price;
        public int _employee;
        public int _customer;
        public int _id;
        
        public Orders() { }

        public Orders(string product, double price, int employee, int customer, int id )
        {
            _product = product;
            _price = price;
            _employee = employee;
            _customer = customer;
            _id = id;
        }

    }
}
