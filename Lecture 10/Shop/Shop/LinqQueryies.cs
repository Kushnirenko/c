﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Shop
{
    class LinqQueryies
    {
        private  List<Employee> listEmployee = new List<Employee>();
        private  List<Customer> listCustomer = new List<Customer>();
        private readonly List<Orders> listOrders = new List<Orders>();
        
        public LinqQueryies()
        {
            listEmployee.Add(new Employee("Петр", "Иванов", "1.09.1990", "менеджер", 1, "PIvanov"));
            listEmployee.Add(new Employee("Иван", "Петров", "1.10.1990", "консультант", 2, "Petrov"));
            listEmployee.Add(new Employee("Сергеев", "Василий", "1.12.1989", "консультант", 3, "Sergeev"));
            listEmployee.Add(new Employee("Николай", "Сухомлин", "31.06.1992", "консультант", 4, "Petrov"));
            listEmployee.Add(new Employee("Сидоров", "Андрей", "7.12.1988", "консультант", 1, "Petrov"));

            listCustomer.Add(new Customer("sid@gmail.com", "096-56-99-777", "Zwebra", "проспект 50 летия октября", 1));
            listCustomer.Add(new Customer("ok@gmail.com", "095-52-98-376", "ItsOk", "Первомайская 11", 2));
            listCustomer.Add(new Customer("Famous@gmail.com", "095-52-98-376", "FamousFirm", "Первомайская 9", 3));
            listCustomer.Add(new Customer("Oreo@gmail.com", "095-52-98-376", "Oreo", "Первомайская 1", 4));
            listCustomer.Add(new Customer("FRF@gmail.com", "095-52-98-376", "FRF", "Первомайская 12", 5));
            
            listOrders.Add(new Orders("notebook", 1500, 1, 2, 1));
            listOrders.Add(new Orders("notebook n122", 2500, 2, 1, 2));
            listOrders.Add(new Orders("notebook n222", 7500, 1, 2, 1));
        }

        public void GetEmployeers()
        {
            var query =
                from ord in listOrders
                join empl in listEmployee on ord._employee equals empl.Id
                select new { firstName = empl.FirstName, LastName = empl.LastName, Product = ord._product,Price = ord._price };

            foreach (var item in query)
            {
                Console.WriteLine("{0,12}  {1,12}  {2,15}  {3,5}",item.firstName,item.LastName,item.Product,item.Price);
            }

        }
        public void GetCustomers()
        {
            var query =
                from ord in listOrders
                join cust in listCustomer on ord._customer equals cust._id
                select new { Name = cust._name,  Product = ord._product, Price = ord._price };

            foreach (var item in query)
            {
                Console.WriteLine("{0,12}  {1,12}  {2,15}", item.Name, item.Product, item.Price);
            }
        }

                   
    }
}
