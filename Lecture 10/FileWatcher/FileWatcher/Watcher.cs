﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Security.Permissions;
using System.Text;
using System.Threading.Tasks;

namespace FileWatcher
{
    class Watcher
    {
        private FileSystemWatcher watcher = new FileSystemWatcher();

        public void Watch(string directory, string typeFile)
        {

            var watcher = new FileSystemWatcher(directory, typeFile);

            watcher.Created += (s, e) =>
            {
                Console.WriteLine("Создан файл {0}", e.Name);
            };

            watcher.Changed += (s, e) =>
            {
                Console.WriteLine("Изменен файл {0}", e.Name);
            };

            watcher.Renamed += (s, e) =>
            {
                Console.WriteLine("Изменено имя файла {0}", e.Name);
            };

            watcher.Deleted += (s, e) =>
            {
                Console.WriteLine("Удален файл {0}", e.Name);
            };

            watcher.EnableRaisingEvents = true;

        }
    }
}
