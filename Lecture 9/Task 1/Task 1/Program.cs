﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Task_1
{
   
    class Program
    {
        static void Main(string[] args)
        {
            Publisher pub = new Publisher();
            Subscriber sub1 = new Subscriber("John ", pub);
            Subscriber sub2 = new Subscriber("George ", pub);
            Subscriber sub3 = new Subscriber("Philip ", pub);
            pub.Create();
            

            Console.WriteLine("Нажмите Enter чтобы закрыть окно.");
            Console.ReadLine();

        }
    }
}
