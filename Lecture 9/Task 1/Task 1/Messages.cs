﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Task_1
{

    public class MessEventArgs : EventArgs
    {
        public MessEventArgs(string s)
        {
            message = s;
        }

        private string message;

        public string Message
        {
            get { return message; }
            set { message = value; }
        }
    } 

    public class Publisher
    {
        public event EventHandler<MessEventArgs> RaiseMessEvent;

        public void Create()
        {
            OnRaiseMessEvent(new MessEventArgs("Привет"));
        }
        protected virtual void OnRaiseMessEvent(MessEventArgs e)
        {
            EventHandler<MessEventArgs> handler = RaiseMessEvent;
            if (handler != null)
            {
                e.Message += String.Format( DateTime.Now.ToString());
                handler(this, e);
            }
        }
    }

    public class Subscriber
    {
        private string id;
        public Subscriber(string ID, Publisher pub)
        {
            id = ID;
            pub.RaiseMessEvent += HandleMessEvent;
        }
        void HandleMessEvent(object sender, MessEventArgs e)
        {
            Console.WriteLine("{0} {1}", id, e.Message);
        }
    }
    public class Listener
    {
        private string _name;
        public Listener(string name,Listener pub)
        {
            _name = name;

           // pub.RaiseMessEvent+=

        }
        void MethodEvent(object s, Listener l)
        {
            
        }
    }


}
