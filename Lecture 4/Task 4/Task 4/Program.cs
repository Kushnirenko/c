﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Task_4
{
    class Program
    {
        static void Main(string[] args)
        {
            int userYear = Convert.ToInt32(Console.ReadLine());
            Console.Clear();
            Cars car = new Cars();
            car.WriteCars(userYear);
            Console.ReadKey();
        }
    }
}
