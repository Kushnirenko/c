﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Task_4
{
    public class Cars:IComparable<Cars>
    {
        private string _brand;
        private string _number;
        private string _owner;
        private int _year;
        private int _run;
        private List<Cars> client =  new List<Cars>();

        public Cars() { }

        public Cars( string brand,string number, string owner,int year, int run )
        {
            _brand = brand;
            _number = number;
            _owner = owner;
            _year = year;
            _run = run;
        }

        public override string ToString()
        {
            return (String.Format("brand of car: {0}\n  car number:{1}\n Owner Name: {2}\n Year: {3}\n Run: {4}\n ", _brand, _number,_owner, _year,_run));
        }

        public void WriteCars(int z)
        {
            client.Add(new Cars("Audi", "AC1777", "Petrov", 1998, 10000));
            client.Add(new Cars("Mercedess", "AC12377", "Ivanov", 2010, 40000));
            client.Add(new Cars("Audi", "AC1789", "Petrov", 1998, 10000));

            foreach (Cars item in client)
            {
                if (item._year < z)
                    Console.WriteLine(item);
            }
        }


        public int CompareTo(Cars other)
        {
            return (_run.CompareTo(other._run));
        }
    }
}
