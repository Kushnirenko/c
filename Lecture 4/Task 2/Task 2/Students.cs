﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Task_2
{
     public class Students:IComparable<Students>
    {
        private string _fio;
        private string _numberGroup;
        private int _mathematic;
        private int _English;
        private int _Phisic;
        public static List<Students> studentList = new List<Students>();
       
        public Students() { }

        public void Init()
        {
            studentList.Add(new Students("Ivanov", "10", 3, 3, 4));
            studentList.Add(new Students("Petrov", "1", 4, 4, 4));
            studentList.Add(new Students("Sidorov", "10", 4, 5, 6));
            studentList.Add(new Students("Petrenko", "1", 3, 5, 5));
        }
         public Students(string fio, string numberGroup, int mathematic, int English, int Phisic)
        {
            _fio = fio;
            _numberGroup = numberGroup;
            _mathematic = mathematic;
            _English = English;
            _Phisic = Phisic;
        }

        public void WriteStudents()
        {
            Console.WriteLine();
            Console.WriteLine(" FIO {0} ",_fio);
            Console.WriteLine(" estimate of math {0}", _mathematic);
            Console.WriteLine(" estimate of English {0} ", _English);
            Console.WriteLine(" estimate of Phisic {0} ", _Phisic);
            Console.WriteLine(" number of group {0}", _numberGroup);
            Console.WriteLine();
        }

        public void GetGoodStudents(string gr)
        {
            foreach (Students item in Students.studentList)
            {
                if (gr == item._numberGroup)
                {
                    if ((item._English + item._mathematic + item._Phisic) / 3 > 3)
                        item.WriteStudents();
                }
            }
        }
        public int CompareTo(Students other)
        {
            return (_numberGroup.CompareTo(other._numberGroup)); ;
        }
    } 
}
