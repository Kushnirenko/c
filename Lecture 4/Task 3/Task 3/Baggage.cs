﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Task_3
{
    public class Baggage:IComparable<Baggage>
    {
        private string _namePas;
        private int _kollVesh;
        private int _ves;

        public static  List<Baggage> bags = new List<Baggage>();

        public override string ToString()
        {
            return String.Format("Name: {0}\n Koll: {1}\n Ves: {2}\n", _namePas, _kollVesh, _ves);
        }

        public Baggage() { }

        public Baggage(string namePas, int kollVesh, int ves)
        {
            _namePas = namePas;
            _kollVesh = kollVesh;
            _ves = ves;
        }
        public void WriteBags(int z)
        {
            bags.Add(new Baggage(" Petrenko", 40, 40));
            bags.Add(new Baggage("Ivanov", 30, 30));
            bags.Add(new Baggage("Pupkin", 10, 25));
            bags.Sort();

            foreach (Baggage item in bags)
            {
                if (item._ves>z)
                {
                    Console.WriteLine(item);
                }
            }

        }

        public int CompareTo(Baggage other)
        {
            return (_kollVesh.CompareTo(other._kollVesh));
        }
}
}
