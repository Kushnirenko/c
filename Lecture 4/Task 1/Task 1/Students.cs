﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Task_1
{
       public class Students : IComparable<Students>
        {
            public static List<Students> students = new List<Students>();
            private string _fio;
            private int _year;
            private string _adress;
            private string _school;

            public Students() { }

            public Students(string fio, int year, string adress, string school)
            {
                _fio = fio;
                _year = year;
                _adress = adress;
                _school = school;
            }

            public void Init()
            {
                students.Add(new Students("Petrov Petr Sergeevich", 1993, "Gagarina 11", "1"));
                students.Add(new Students("Ivanov Sergey Petrovich", 1991, "Sadovaya 11", "11"));
                students.Add(new Students("Petrenko Vasiliy Dmitriecich", 1993, "Gagarina 11", "11"));
                students.Add(new Students("Vasilev", 1992, "sadova street", "11"));
            }

            public void WriteStudents()
            {
                Console.WriteLine();
                Console.WriteLine("Fio  {0}", _fio);
                Console.WriteLine("Year {0} \t", _year);
                Console.WriteLine("adress {0} \t ", _adress);
                Console.WriteLine("number of school {0} \t", _school);
                Console.WriteLine();
            }

            public static void SearchStudents(string userSchool)
            {
                foreach (Students stud in students)
                {
                    if (userSchool == stud)
                        stud.WriteStudents();
                }
            }
            public int CompareTo(Students other)
            {
                return (_year.CompareTo(other._year));
            }

            public static bool operator ==(string School, Students student)
            {
                return School == student._school;

            }
            public static bool operator !=(string School, Students student)
            {
                return School != student;
            }
        }
}
